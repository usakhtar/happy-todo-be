package todoService

var todoItemsList = []string{}

type Item struct {
	ItemName string `json:"item"`
}


func GetTodoItems() []string {
	return todoItemsList
}

func AddTodoItem(item string) {
	todoItemsList = append(todoItemsList, item)
}
