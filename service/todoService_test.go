package todoService

import (
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func TestAcceptance(t *testing.T) {
	AddTodoItem("Buy some milk")
	resp, err := http.Get("http://private-a96d5-todo150.apiary-mock.com/todos")
	if err != nil {
		t.Errorf("Could not call request")
	}
	body, err := ioutil.ReadAll(resp.Body)
	sb := string(body)

	if len(GetTodoItems()) == 0 {
		t.Errorf("Could not get todo items")
	} else if !strings.Contains(sb, GetTodoItems()[0]) {
		t.Errorf("Could not get todo items")
	}
}

func TestUnitAddNewItem(t *testing.T) {
	AddTodoItem("Buy some milk")
	if len(GetTodoItems()) == 0 {
		t.Errorf("Could not get todo items")
	} else if GetTodoItems()[0] != "Buy some milk" {
		t.Errorf("Could not get todo items")
	}
}
