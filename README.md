# Happy ToDo Application Backend

Happy Todo is a simple todo application to demonstrate how to build application in go using A-TDD approach.


## Acceptance Test-Driven Development (A-TDD)
The application is build following A-TDD approach, while developing project acceptance test was first written in accordance with apiblueprint to ensure contract.After that unit tests were written followed by their implementation.
## Program Structure
![Program Structure](https://i.ibb.co/Fh71s4d/structure.png)

## Installation
Use command 

`go get` 

to retrieve all the packages being used.

## Running
Server can be started by simply just executing command

`go run main.go .`

By default server starts on port `8080`.

## Testing
As application is built focusing on A-TDD. So acceptance and contract tests are performed by using apiblueprint tool called [https://apiary.io/](https://apiary.io/). 

The apiblueprint being used is

![apiary](https://i.ibb.co/7VS37Yw/apiary.png)

and can be accessed by public url
[http://private-a96d5-todo150.apiary-mock.com/todos](http://private-a96d5-todo150.apiary-mock.com/todos)


In order to run tests simply execute command

`go test .
`
## Deployment

#### Docker

The application image can be created using docker as

`docker build -t happy-todo-be .`

and then to run container

`docker run -p 8080:8080 happy-todo-be`

#### CI|CD
The CI|CD pipeline has four stages 

![pipeline](https://i.ibb.co/L5fLk7H/Screenshot-2021-11-18-at-11-14-29-PM.png)

##### build
The goal of this stage is to ensure app is being build properly.

##### test
The goal of this stage is to perform testing.

##### dockerize
The goal of this stage is to create docker image of an app and deploy it on Elastic Container Registry (ECR).

Tools required to complete this stage are
- Docker
- Elastic Container Registry
- awscli
- eksctl

The ECR repositry being used is:

![repositry](https://i.ibb.co/zxBgYpL/Screenshot-2021-11-18-at-11-23-39-PM.png) 

The repositry can be created using AWS Cli using command

`aws ecr create-repository --repository-name happy-todo-be --region ap-southeast-1`


##### deploy
The goal of this stage is to deploy docker image from Elastic Container Registry (ECR) to Amazon Elastic Kubernetes.

Tools required to complete this stage are
- Amazon Elastic Kubernetes
- awscli
- eksctl

Before this stage we have to make sure that cluster is properly configured. 
To configure cluster properly we need to perform some 3 steps

1. Create cluster

The cluster can be created by eksctl simply as

`eksctl create cluster --name happy-todo-be-cluster --region ap-southeast-1 --fargate`

![cluster](https://i.ibb.co/RTmb9Sg/Screenshot-2021-11-18-at-11-32-13-PM.png) 

2. Deploy

Deployment can be done using command

`kubectl apply -f deployment.yaml`

after the successfull execution of above command pod will be started.

3. Start service

Service can be started using command

`kubectl apply -f service.yaml`

## Rest Endpoints

### List All Todo Items

/todos

Request Method: GET

Response: 

```
  [
     "Buy some milk"
  ]
```

### Create Item

/todos

Request Method: POST

Request Body
```
  {
     "item": "Buy soda"
  }
```



