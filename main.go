package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"happy-todo/service"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/todos", GetTodoItemsCall).Methods("GET")
	router.HandleFunc("/todos", AddNewItemCall).Methods("POST", "OPTIONS")
	http.Handle("/", router)
	http.ListenAndServe(":8080", router)
}

func GetTodoItemsCall(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	jsonResponse, err := json.Marshal(todoService.GetTodoItems())
	if err != nil {
		return
	}

	w.Write(jsonResponse)
}

func AddNewItemCall(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var item todoService.Item
	_ = json.NewDecoder(r.Body).Decode(&item)
	todoService.AddTodoItem(item.ItemName)
	json.NewEncoder(w).Encode(&item)
}
